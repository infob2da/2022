---
layout: home
title: Home
menu: Home
order: 1
---

<img src="assets/i/teaser.png">
<!-- <div class="credits"><a href="http://mbostock.github.io/d3/talk/20111116/bundle.html">Hierarchical edge bundling]</a> | <a href="http://hint.fm/wind/">Wind map</a> | <a href="http://www.nytimes.com/interactive/2012/10/15/us/politics/swing-history.html?_r=0">How states have shifted</a> </div> -->

## INFOB2DA 2022

Applied data analytics is a **multidisciplinary field** where you will learn insights needed to make sense of data, research, and observations from everyday life.

You will learn how to apply a **data-driven approach to problem-solving**, but will not only learn about tools, methods, and techniques, or the latest trends, but also more generic insights: why do certain approaches work, why the field is so popular, what common mistakes are made.

The lectures will provide the theoretical background of how a data analytics process should be performed.
Furthermore, we discuss an overview of **popular data analytics and visualization techniques** to help match techniques with information needs, including applications of text mining and data enrichment.

## Content

- Fundamental Data Mining Methods
- Data Preparation and Preprocessing
- Common Analysis Algorithms and Methods
- Principles of Information Visualization
- Human Perception and Visualization Design
- Data Visualization Techniques for Particular Data Types

The lecture is separated in three parts. Part one deals with the principal data understanding methods, the second and main focus lies on automatic data preprocessing, cluster & outlier analysis techniques, classification and association rules. Subject of the third part are the basics of information visualization, the foundations of human perception and user interface design.

## Course Sessions and NEWS

> Update 05.07.2022: The initial 2022 website content has been uploaded; text still under revision

> Update 01.07.2022: INFOB2DA has been scheduled to **Block 1 Timeslot C**  
> with Monday afternoon (Werkcolleges), Tue afternoon (lecture), Thursday morning (lecture).  
> UU's Block and timeslot information can be found [here](https://students.uu.nl/en/hum/exchange-students/schedules-calendar-and-timeslots).

> Update 16.08.2022: Added time slot and location for lectures.

> Update 05.09.2022: Corrected information on Thursday lectures and noted exception for Thu Sep 8th.

<!-- > Update 24.08.2021: All INFOB2DA will happen on-site in accordance to the new [UU onderwijs rules](https://www.uu.nl/en/news/cvb-update-coronavirus-18-augustus) published on 18.08.2021.

> **The 2021 has been moved from Block 2 to Block 1 and will happen in Timeslot C** (06-09-2021 to 12-11-2021, Monday afternoon, Tuesday afternoon, Thursday morning).
> UU's Block and timeslot information can be found [here](https://students.uu.nl/en/hum/exchange-students/schedules-calendar-and-timeslots). -->

<!-- > Due to the Covid-19 situation our lectures will be held fully online, while we plan to offer Tutorials/Assigments/Labs (werkcollege) (partially) on site.
> Be aware: The related information might be adapted on-the-fly to the current situation. All updates will be communicated through MS Teams. -->

<!-- <span style="color:red"> The 2021 has been rescheduled from Timeslot C to Timeslot B</span> -->

### Lectures:

The location of the on-site lectures is still subject to change.

Tuesdays 15:15 - 17:00, Location: [On SITE Location BOL - 1.065](https://students.uu.nl/bolognalaan-101) \
Thursdays 11:00 - 12:45, Location: [On SITE Location BOL - 1.065](https://students.uu.nl/bolognalaan-101). \
**Exception**: Thursday September 8th the course will be held **on-line** through MS Teams.

### Tutorials/Assigments/Labs (werkcollege):

Mondays, from 17:15 - 19:00, Location: Check MS Teams, in your TA's specific channel.

<!-- _Due to the Covid-19 situation our lab locations will changing on the fly and will be announced (per group) on an individual basis through MS Teams_

_Group 1/2/3/4/(5):_ Monday 17:15 - 19:00, Location: [On SITE Location - BOL - 1.075/1.128/1.138/3.108/0.202/2.100](https://students.uu.nl/bolognalaan-101)
_Group Online 1 (pot. more):_ Monday 17:15 - 19:00, Location: [Online, MS Teams]({{site.teamsurl}})

<!-- _Group 5:_ Thursday 11:00 - 12:45, Location: [On SITE Location - To Be Announced, all groups individually](https://students.uu.nl/buys-ballotgebouw)
_Group 6:_ Thursday 11:00 - 12:45, Location: [On SITE Location - To Be Announced, all groups individually](https://students.uu.nl/buys-ballotgebouw)
_Group 7:_ Thursday 11:00 - 12:45, Location: [On SITE Location - To Be Announced, all groups individually](https://students.uu.nl/buys-ballotgebouw) -->

### Office Hours:

Office hours are posted [here]({{ site.baseurl }}/schedule).

**Lecture Resources:**  
Discussion forum on [MS Teams (Discussion Channel)]({{ site.teamsurl }})  
Materials and grades also on [MS Teams (General -> Files)]({{ site.teamsurl }})

**Workload:**

7.5 ECTS-Credits for lecture, tutorials, labs, and homeworks; Representing in total 210 hours, split into

- 50 hours course of study with attendance
- 160 hours of self-study time

## Instructor and Head TF

[Michael Behrisch](http://michael.behrisch.info) (Instructor) \
Alister Machado dos Reis (Head TF)

### Teaching Fellows

You should be allocated to one of the following TA's channels in our MS Teams.

Floris Copraij \
Elio Verhoef \
Tim Smit \
Selim Büyük \
Vincent Haverhoek

<!-- - _Group 1_ Jasper van Winkelhoff
- _Group 2_ Selim Büyük
- _Group 3_ Hessel Laman
- _Group 4_ Yoram Frenkiel
- _Group 5_ Lisanne Koetsier
  (group allocation subject to change) -->

### COVID-19 Rules for this Class

We are following the [Utrecht University COVID-19 Rules](https://www.uu.nl/en/information-coronavirus).
Generally, we will keep the work as remote as possible, while still trying to foster community building aspect.

**Lectures** will be held ON-SITE; The _Labs/Werkcolleges_ are currently planned to be ON-SITE.  
_Please be aware that this information can change rapidly._

### Previous Years (Archive)

[INFOB2DA 2020 Website](https://infob2da.gitlab.io/2020/)

[INFOB2DA 2021 Website](https://infob2da.gitlab.io/2021/)

<!--
[2017 Fall Website](http://www.cs171.org/2017/)

[2016 Fall Website](http://www.cs171.org/2016/)

[2016 Spring Website](http://www.cs171.org/2016_Spring/)

[2015 Website](http://www.cs171.org/2015/)
[2015 Video Archive](http://cm.dce.harvard.edu/2015/02/24028/publicationListing.shtml)

[2014 Website](http://www.cs171.org/2014/)
[2014 Video Archive](http://cm.dce.harvard.edu/2014/02/24028/publicationListing.shtml)

[2013 Video Archive](http://cm.dce.harvard.edu/2013/02/22872/publicationListing.shtml)

[2012 Video Archive](http://cm.dce.harvard.edu/2012/02/22872/publicationListing.shtml) -->
